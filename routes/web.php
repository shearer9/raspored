<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::post('/studentsSchedule', 'HomeController@studentsSchedule');
Route::get('/subjectChoice', 'HomeController@subjectChoice');
Route::post('/subjectChoice', 'HomeController@attachSubject');
Route::get('/professors', 'HomeController@professors');

Route::get('/messages/{schoolClass}', 'MessageController@create');
Route::post('/messages/{schoolClass}', 'MessageController@store');

Route::get('/teachers', 'TeacherController@index');

Route::post('/schedules/class', 'ScheduleController@classSchedule');
Route::resource('schedules', 'ScheduleController');
Route::resource('schoolClasses', 'SchoolClassController');
Route::post('/subjects', 'SubjectController@store');

Route::post('/classDepartments', 'ClassDepartmentController@store');



