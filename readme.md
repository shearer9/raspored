# Raspored (Schedule)

This is a Laravel project in which I made simple schedule for schools. 

I used Laragon local development environment (PHP,MySQL,Nginx) but feel free to use whatever you like.

## Installation


```bash
# install dependencies
composer install

# create database (ex. schedule)

# edit .env
rename .env.example to .env and set right credentials
(DB_DATABASE, DB_USERNAME, DB_PASSWORD)

# set application key for encryption
php artisan key:generate

# fill database
php artisan migrate --seed

# serve file
php artisan serve
```

## Usage

Admin - Person responsible for organizing schedule, registering new professors and has the option of receiving notifications from professors so they can rearrange schedule accordingly

```bash
E-mail: admin@admin.com
Password: 123456
```
Logged users would be professors who could see their own schedule for upcoming week.
They would also have the option of notifying the admin.

Guests would be mostly students who could choose the class they are in and review the schedule for next week.



## License
[MIT](https://choosealicense.com/licenses/mit/)