@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row my-4 justify-content-center">
        <div class="col-md-10">
            <h3 class="mb-3 text-center">Raspored sati za tjedan: {{$raspored->date_from}} - {{$raspored->date_to}}</h3>
            <table class="table table-bordered">
                <thead class="thead-light">
                    <tr>
                      <th scope="col" class="text-center">Ponedjeljak</th>
                      <th scope="col" class="text-center">Utorak</th>
                      <th scope="col" class="text-center">Srijeda</th>
                      <th scope="col" class="text-center">Cetvrtak</th>
                      <th scope="col" class="text-center">Petak</th>
                      <th scope="col" class="text-center">Subota</th>
                      <th scope="col" class="text-center">Nedjelja</th>
                    </tr>
                </thead>
                <tbody>
                    @for($i = 1; $i <= 7; $i++)
                    <tr>
                        @for($ii = 1; $ii <= 7; $ii++)
                            @foreach($skolskiSati as $sat)
                                @if($sat->number == $i && $sat->day_number == $ii)
                                <td class="text-center"><a href="/messages/{{$sat->id}}">{{$sat->subjectTeacher->subject->name}}<br>
                                Razred: {{$sat->classDepartment->department}}<br>
                                Učionica: {{$sat->classroom}} </a></td>
                                @break
                                @elseif($loop->last)
                                <td class="text-center"></td>
                                @endif
                            @endforeach
                        @endfor
                    </tr>
                    @endfor
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection