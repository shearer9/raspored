@extends('layouts.app')

@section('content')
        <div id="home" class="container-fluid justify-content-center">
                <h1 class="text-white text-center mb-5">Dobrodošli</h1>
                @if($raspored !== null)
                <div class="container">
                <h4 class="text-white my-4">Izaberite razred u kojem se nalazite:</h4>
                    <div class="form-group col-4">
                        <form method="post" action="/studentsSchedule">
                            @csrf
                        <label class="text-light" for="class_department">Raspored za tjedan: {{$raspored->date_from}} - {{$raspored->date_to}}</label>
                        <select name="class_department_id" class="form-control" id="class_department">
                        @foreach($classDepartments as $classDepartment)
                        <option value="{{$classDepartment->id}}">{{$classDepartment->department}}</option>
                        @endforeach
                        </select>
                        <button type="submit" class="mt-3 col-4 btn btn-primary">Nastavi</button>
                        </form>
                    </div>
                </div>
                @endif

 </div>
 <div class="jumbotron p-2 my-3">
    <h3 class="display-4 text-center">{{$quote->number}}</h3>
        <p class="lead text-center">{{$quote->text}}</p>
          <hr class="my-4">
          <p class="text-center"><i>Zanimljive činjenice o brojevima</i></p>
</div>
@endsection
