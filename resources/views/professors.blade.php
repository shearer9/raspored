@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row my-4 justify-content-center">
        <div class="col-6">
            <h3 class="text-center mb-3">Pregled svih profesora:</h3>
            <table id="professorsTable" class="table table-hover table-bordered">
              <thead class="thead-light">
                <tr class="text-center">
                  <th scope="col">Profesor</th>
                  <th scope="col">Predmeti</th>
                </tr>
              </thead>
              <tbody>
                @foreach($professors as $professor)
                <tr>
                    <td class="text-center">
                    {{$professor->user->name}}
                    </td>
                    <td class="text-center">
                    @foreach($professor->subjects as $subject)
                    {{$subject->name}},
                    @endforeach
                    </td>
                </tr>
                @endforeach
              </tbody>
            </table>
            <button class="btn btn-secondary" data-toggle="modal" data-target="#exampleModal">Dodaj novi predmet</button>
        </div>
    </div>
</div>

{{-- Modal --}}
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalTitle">Dodaj novi predmet</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="forma" method="post" action="/subjects">
            @csrf
            <label for="name">Naziv predmeta:</label>
            <input class="form-control col-8" type="text" id="name" name="name">
        </form>
       </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Zatvori</button>
        <button type="submit" form="forma" class="btn btn-primary">Spremi predmet</button>
      </div>
    </div>
  </div>
</div>
@endsection

