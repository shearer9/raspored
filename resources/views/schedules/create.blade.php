@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row my-4 justify-content-center">
        <div class="col-6">
            <h3 class="text-center mb-3 text-muted">Dodajte novi raspored</h3>
            <form method="post" action="/schedules">
            @csrf
                <div class="form-group">
                <label for="date_from">Datum od:</label>
                    <input class="form-control" id="date_from" name="date_from" type="date">
                </div>
                <div class="form-group">
                <label for="date_to">Datum od:</label>
                    <input class="form-control" id="date_to" name="date_to" type="date">
                </div>
                <div class="form-group">
                <label for="shift">Smjena</label>
                    <select class="form-control" id="shift" name="shift">
                        <option value="1">1</option>
                        <option value="2">2</option>
                    </select>
                <button type="submit" class="btn btn-primary form-control mt-3">Potvrdi</button>
            </form>
        </div>
    </div>
</div>
@endsection

