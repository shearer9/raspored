@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row my-4 justify-content-center">
        <div class="col-md-10">
            <h3 class="mb-1 text-center">Raspored sati za tjedan: {{$schedule->date_from}} - {{$schedule->date_to}}</h3> <br>
            <h4 class="text-center mb-3">Razred: {{$currentClass->department}}</h4>
            <table class="table table-bordered">
                <thead class="thead-light">
                    <tr>
                      <th scope="col" class="text-center">Ponedjeljak</th>
                      <th scope="col" class="text-center">Utorak</th>
                      <th scope="col" class="text-center">Srijeda</th>
                      <th scope="col" class="text-center">Cetvrtak</th>
                      <th scope="col" class="text-center">Petak</th>
                      <th scope="col" class="text-center">Subota</th>
                      <th scope="col" class="text-center">Nedjelja</th>
                    </tr>
                </thead>
                <tbody>
                    @for($i = 1; $i <= 7; $i++)
                    <tr>
                        @for($ii = 1; $ii <= 7; $ii++)
                            @foreach($schoolClasses as $class)
                                @if($class->number == $i && $class->day_number == $ii)
                                <td class="text-center"><a href="/schoolClasses/{{$class->id}}">{{$class->subjectTeacher->subject->name}}<br>
                                Učionica: {{$class->classroom}}</a></td>
                                @break
                                @elseif($loop->last)
                                <td class="text-center"></td>
                                @endif
                            @endforeach
                        @endfor
                    </tr>
                    @endfor
                </tbody>
            </table>
            <a class="btn btn-primary" role="button" href="/schoolClasses/create?schedule={{$schedule->id}}&class={{$currentClass->id}}">Novi školski sat</a>
        </div>
    </div>
</div>
@endsection
