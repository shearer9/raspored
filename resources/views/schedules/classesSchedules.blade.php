@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row my-4 justify-content-center">
        <form method="post" action="/schedules/class">
            @csrf
            <div class="form-group">
                <h3 class="mb-4">Izaberite razred:</h3>
                <select class="form-control" name="class">
                @foreach($classes as $class)
                    <option value="{{$class->id}}">{{$class->department}}</option>
                @endforeach
                </select>
                @if(!$classes->isEmpty())
                <button type="submit" class="btn btn-primary form-control my-4">Potvrdi</button>
                @endif
                <button type="button" class="btn btn-secondary form-control my-4" data-toggle="modal" data-target="#exampleModal">
                    Dodaj novo odjeljenje
                </button>
                <button type="button" class="btn btn-danger form-control" data-toggle="modal" data-target="#exampleModal2">
                    Izbriši raspored
                </button>
                <input type="hidden" name="scheduleId" value="{{request()->route('schedule.id')}}">
            </div>
        </form>
    </div>
</div>

{{-- Modal za novo odjeljenje --}}
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalTitle">Dodaj novo odjeljenje</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="forma" method="post" action="/classDepartments">
            @csrf
            <label for="department">Razred:</label>
            <input type="text" id="department" name="department">
        </form>
       </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Zatvori</button>
        <button type="submit" form="forma" class="btn btn-primary">Spremi razred</button>
      </div>
    </div>
  </div>
</div>

{{-- Modal za brisanje rasporeda --}}
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Jeste li sigurni da želite izbrisati raspored?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="forma2" method="post" action="/schedules/{{request()->route('schedule.id')}}">
            @csrf
            @method('DELETE')
        </form>
        Brisanjem rasporeda ćete izbrisati i sve povezane školske sate
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Zatvori</button>
        <button type="submit" form="forma2" class="btn btn-danger">Izbriši</button>
      </div>
    </div>
  </div>
</div>

@endsection