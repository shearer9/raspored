@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row my-4 justify-content-center">
        <div class="col-6">
            <h3 class="text-center mb-3">Pregled svih rasporeda:</h3>
            <div class="list-group">
                @foreach($schedules as $schedule)
                    <a href="/schedules/{{$schedule->id}}" class="list-group-item list-group-item-action text-center">
                    {{$schedule->date_from}} - {{$schedule->date_to}}
                    </a>
                @endforeach
            </div>
            <a class="btn btn-primary mt-4 float-right" href="/schedules/create" role="button">Novi raspored</a>
        </div>
    </div>
</div>
@endsection

