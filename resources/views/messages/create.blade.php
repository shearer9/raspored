@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row my-4 justify-content-center">
        <form method="POST" action="/messages/{{$schoolClass->id}}">
            {{    csrf_field()  }}
            <div class="form-group">
                <label for="exampleFormControlTextarea1">Pošaljite poruku:</label>
                <textarea name="text" class="form-control" id="exampleFormControlTextarea1" cols="45" rows="5"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Potvrdi</button>
        </form>
    </div>
</div>
@endsection