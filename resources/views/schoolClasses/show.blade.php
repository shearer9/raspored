@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row my-4 justify-content-center">
        <div class="col-5 card text-center">
            <div class="card-body">
                <h5 class="card-title">Školski sat</h5>
                <hr>
                <p class="card-text">Redni broj: {{$schoolClass->number}}</p>
                <p class="card-text">Predmet: {{$schoolClass->subjectTeacher->subject->name}}</p>
                <p class="card-text">Profesor: {{$schoolClass->subjectTeacher->teacher->user->name}}</p>
                <p class="card-text">Učionica: {{$schoolClass->classroom}}</p>
                <hr>
                <p class="card-text"> {{$schoolClass->classDepartment->department}}</p>
                <div class="row d-flex justify-content-center mt-3">
                    <form id="forma" method="post" action="/schoolClasses/{{$schoolClass->id}}">
                        @csrf
                        @method('DELETE')
                    </form>
                    <button type="submit" form="forma" class="btn btn-danger">Izbriši</button>
                </div>
          </div>
        </div>
    </div>

@endsection