@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row my-4 justify-content-center">
        <div class="col-5">
        <form method="post" action="/schoolClasses">
            @csrf
            @if($flash = session('message'))
            <div class="mt-2 alert alert-success">{{$flash}}</div>
            @endif
            <h3 class="mb-4 text-center">Kreirajte novi školski sat</h3>
            <h5 class="text-center mb-3">Razred: {{$class->department}}</h5>
                <div class="form-group">
                <label for="day_number">Dan</label>
                    <select class="form-control" id="day_number" name="day_number">
                        <option value="1">Ponedjeljak</option>
                        <option value="2">Utorak</option>
                        <option value="3">Srijeda</option>
                        <option value="4">Četvrtak</option>
                        <option value="5">Petak</option>
                        <option value="6">Subota</option>
                        <option value="7">Nedjelja</option>
                    </select>
                </div>
                <div class="form-group">
                <label for="number">Broj sata</label>
                    <select class="form-control" id="number" name="number">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                    </select>
                </div>
                <div class="form-group">
                <label for="subjectTeacher">Izaberite predmet i profesora</label>
                    <select class="form-control" id="subjectTeacher" name="subject_teacher_id">
                    @foreach($subjectTeachers as $subjectTeacher)
                    <option value="{{$subjectTeacher->id}}">{{$subjectTeacher->teacher->user->name}} - [{{$subjectTeacher->subject->name}}]</option>
                    @endforeach
                    </select>
                </div>
                <div class="form-group">
                <label for="classroom">Broj učionice</label>
                    <input type="number" class="form-control" id="classroom" name="classroom">
                </div>
                <input type="hidden" name="class_department_id" value="{{$class->id}}">
                <input type="hidden" name="schedule_id" value="{{$schedule}}">
                <button type="submit" class="btn btn-primary form-control mt-3">Potvrdi</button>
        </form>
        @include('errors')
        </div>

    </div>
</div>


@endsection