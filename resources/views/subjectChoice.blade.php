@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header text-center text-light bg-secondary">Izaberite predmet</div>

                <div class="card-body shadow-lg">
                    <form method="post" action="/subjectChoice">
                        @csrf
                        <div class="p-3">
                        <div class="form-group row">
                        <label for="subjects" class="col-sm-3 col-form-label">Izaberite predmet</label>
                            <select multiple class="form-control col-sm-9" id="subjects" name="subjects[]">
                                @foreach($subjects as $subject)
                                <option value="{{$subject->id}}">{{$subject->name}}</option>
                                @endforeach
                            </select>
                            <small>Držite CTRL ako želite izabrati više predmeta</small>
                        </div>
                        <button type="submit" class="btn btn-primary">Potvrdi</button>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection