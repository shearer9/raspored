<?php

use Illuminate\Database\Seeder;

class SubjectsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('subjects')->delete();
        
        \DB::table('subjects')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Hrvatski',
                'created_at' => '2018-10-16 23:41:43',
                'updated_at' => '2018-10-16 23:41:43',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Matematika',
                'created_at' => '2018-10-16 23:41:52',
                'updated_at' => '2018-10-16 23:41:52',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Matematika 2',
                'created_at' => '2018-10-16 23:41:52',
                'updated_at' => '2018-10-16 23:41:52',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'TZK',
                'created_at' => '2018-10-16 23:41:52',
                'updated_at' => '2018-10-16 23:41:52',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'OET',
                'created_at' => '2018-10-16 23:41:52',
                'updated_at' => '2018-10-16 23:41:52',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Računarstvo',
                'created_at' => '2018-10-16 23:41:52',
                'updated_at' => '2018-10-16 23:41:52',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Likovno',
                'created_at' => '2018-10-16 23:41:52',
                'updated_at' => '2018-10-16 23:41:52',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Geografija',
                'created_at' => '2018-10-16 23:41:52',
                'updated_at' => '2018-10-16 23:41:52',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Povijest',
                'created_at' => '2018-10-16 23:41:52',
                'updated_at' => '2018-10-16 23:41:52',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Biologija',
                'created_at' => '2018-10-16 23:41:52',
                'updated_at' => '2018-10-16 23:41:52',
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'Elektronika',
                'created_at' => '2018-10-16 23:41:52',
                'updated_at' => '2018-10-16 23:41:52',
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'Glazbeno',
                'created_at' => '2018-10-18 14:43:58',
                'updated_at' => '2018-10-18 14:43:58',
            ),
        ));
        
        
    }
}