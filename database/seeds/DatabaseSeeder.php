<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(SubjectsTableSeeder::class);
        $this->call(TeachersTableSeeder::class);
        $this->call(SubjectTeacherTableSeeder::class);
        $this->call(SchedulesTableSeeder::class);
        $this->call(SchoolClassesTableSeeder::class);
        $this->call(ClassDepartmentsTableSeeder::class);
    }
}
