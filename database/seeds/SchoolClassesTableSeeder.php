<?php

use Illuminate\Database\Seeder;

class SchoolClassesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('school_classes')->delete();
        
        \DB::table('school_classes')->insert(array (
            0 => 
            array (
                'id' => 1,
                'number' => 1,
                'subject_teacher_id' => 1,
                'schedule_id' => 1,
                'classroom' => 100,
                'class_department_id' => 1,
                'day_number' => 1,
                'created_at' => '2018-10-17 15:37:17',
                'updated_at' => '2018-10-17 15:37:17',
            ),
            1 => 
            array (
                'id' => 3,
                'number' => 3,
                'subject_teacher_id' => 14,
                'schedule_id' => 1,
                'classroom' => 111,
                'class_department_id' => 1,
                'day_number' => 1,
                'created_at' => '2018-10-17 16:08:41',
                'updated_at' => '2018-10-17 16:08:41',
            ),
            2 => 
            array (
                'id' => 4,
                'number' => 4,
                'subject_teacher_id' => 13,
                'schedule_id' => 1,
                'classroom' => 121,
                'class_department_id' => 1,
                'day_number' => 1,
                'created_at' => '2018-10-17 16:08:48',
                'updated_at' => '2018-10-17 16:08:48',
            ),
            3 => 
            array (
                'id' => 5,
                'number' => 5,
                'subject_teacher_id' => 8,
                'schedule_id' => 1,
                'classroom' => 111,
                'class_department_id' => 1,
                'day_number' => 1,
                'created_at' => '2018-10-17 16:08:55',
                'updated_at' => '2018-10-17 16:08:55',
            ),
            4 => 
            array (
                'id' => 8,
                'number' => 3,
                'subject_teacher_id' => 10,
                'schedule_id' => 1,
                'classroom' => 211,
                'class_department_id' => 1,
                'day_number' => 2,
                'created_at' => '2018-10-17 16:09:43',
                'updated_at' => '2018-10-17 16:09:43',
            ),
            5 => 
            array (
                'id' => 9,
                'number' => 4,
                'subject_teacher_id' => 11,
                'schedule_id' => 1,
                'classroom' => 255,
                'class_department_id' => 1,
                'day_number' => 2,
                'created_at' => '2018-10-17 16:09:53',
                'updated_at' => '2018-10-17 16:09:53',
            ),
            6 => 
            array (
                'id' => 11,
                'number' => 2,
                'subject_teacher_id' => 5,
                'schedule_id' => 1,
                'classroom' => 111,
                'class_department_id' => 1,
                'day_number' => 3,
                'created_at' => '2018-10-17 16:11:27',
                'updated_at' => '2018-10-17 16:11:27',
            ),
            7 => 
            array (
                'id' => 12,
                'number' => 3,
                'subject_teacher_id' => 3,
                'schedule_id' => 1,
                'classroom' => 122,
                'class_department_id' => 1,
                'day_number' => 3,
                'created_at' => '2018-10-17 16:11:36',
                'updated_at' => '2018-10-17 16:11:36',
            ),
            8 => 
            array (
                'id' => 13,
                'number' => 4,
                'subject_teacher_id' => 14,
                'schedule_id' => 1,
                'classroom' => 100,
                'class_department_id' => 1,
                'day_number' => 3,
                'created_at' => '2018-10-17 16:11:54',
                'updated_at' => '2018-10-17 16:11:54',
            ),
            9 => 
            array (
                'id' => 27,
                'number' => 1,
                'subject_teacher_id' => 9,
                'schedule_id' => 1,
                'classroom' => 111,
                'class_department_id' => 1,
                'day_number' => 2,
                'created_at' => '2018-10-18 16:08:16',
                'updated_at' => '2018-10-18 16:08:16',
            ),
            10 => 
            array (
                'id' => 28,
                'number' => 1,
                'subject_teacher_id' => 14,
                'schedule_id' => 1,
                'classroom' => 211,
                'class_department_id' => 1,
                'day_number' => 3,
                'created_at' => '2018-10-18 16:09:03',
                'updated_at' => '2018-10-18 16:09:03',
            ),
            11 => 
            array (
                'id' => 29,
                'number' => 2,
                'subject_teacher_id' => 15,
                'schedule_id' => 1,
                'classroom' => 112,
                'class_department_id' => 1,
                'day_number' => 1,
                'created_at' => '2018-10-18 16:15:57',
                'updated_at' => '2018-10-18 16:15:57',
            ),
            12 => 
            array (
                'id' => 30,
                'number' => 5,
                'subject_teacher_id' => 13,
                'schedule_id' => 1,
                'classroom' => 200,
                'class_department_id' => 1,
                'day_number' => 2,
                'created_at' => '2018-10-18 16:31:48',
                'updated_at' => '2018-10-18 16:31:48',
            ),
            13 => 
            array (
                'id' => 31,
                'number' => 5,
                'subject_teacher_id' => 2,
                'schedule_id' => 1,
                'classroom' => 111,
                'class_department_id' => 1,
                'day_number' => 3,
                'created_at' => '2018-10-18 16:32:16',
                'updated_at' => '2018-10-18 16:32:16',
            ),
            14 => 
            array (
                'id' => 32,
                'number' => 6,
                'subject_teacher_id' => 8,
                'schedule_id' => 1,
                'classroom' => 200,
                'class_department_id' => 1,
                'day_number' => 2,
                'created_at' => '2018-10-18 16:33:06',
                'updated_at' => '2018-10-18 16:33:06',
            ),
            15 => 
            array (
                'id' => 33,
                'number' => 2,
                'subject_teacher_id' => 3,
                'schedule_id' => 1,
                'classroom' => 211,
                'class_department_id' => 1,
                'day_number' => 2,
                'created_at' => '2018-10-18 16:33:55',
                'updated_at' => '2018-10-18 16:33:55',
            ),
            16 => 
            array (
                'id' => 34,
                'number' => 2,
                'subject_teacher_id' => 3,
                'schedule_id' => 1,
                'classroom' => 222,
                'class_department_id' => 1,
                'day_number' => 4,
                'created_at' => '2018-10-18 16:52:10',
                'updated_at' => '2018-10-18 16:52:10',
            ),
            17 => 
            array (
                'id' => 35,
                'number' => 1,
                'subject_teacher_id' => 5,
                'schedule_id' => 1,
                'classroom' => 250,
                'class_department_id' => 1,
                'day_number' => 4,
                'created_at' => '2018-10-18 17:59:04',
                'updated_at' => '2018-10-18 17:59:04',
            ),
            18 => 
            array (
                'id' => 36,
                'number' => 2,
                'subject_teacher_id' => 1,
                'schedule_id' => 1,
                'classroom' => 200,
                'class_department_id' => 2,
                'day_number' => 1,
                'created_at' => '2018-10-18 18:03:47',
                'updated_at' => '2018-10-18 18:03:47',
            ),
            19 => 
            array (
                'id' => 37,
                'number' => 1,
                'subject_teacher_id' => 14,
                'schedule_id' => 1,
                'classroom' => 250,
                'class_department_id' => 2,
                'day_number' => 1,
                'created_at' => '2018-10-18 18:03:54',
                'updated_at' => '2018-10-18 18:03:54',
            ),
            20 => 
            array (
                'id' => 38,
                'number' => 3,
                'subject_teacher_id' => 13,
                'schedule_id' => 1,
                'classroom' => 200,
                'class_department_id' => 2,
                'day_number' => 1,
                'created_at' => '2018-10-18 18:04:01',
                'updated_at' => '2018-10-18 18:04:01',
            ),
            21 => 
            array (
                'id' => 39,
                'number' => 4,
                'subject_teacher_id' => 2,
                'schedule_id' => 1,
                'classroom' => 111,
                'class_department_id' => 2,
                'day_number' => 1,
                'created_at' => '2018-10-18 18:04:12',
                'updated_at' => '2018-10-18 18:04:12',
            ),
            22 => 
            array (
                'id' => 40,
                'number' => 5,
                'subject_teacher_id' => 4,
                'schedule_id' => 1,
                'classroom' => 200,
                'class_department_id' => 2,
                'day_number' => 1,
                'created_at' => '2018-10-18 18:04:23',
                'updated_at' => '2018-10-18 18:04:23',
            ),
            23 => 
            array (
                'id' => 41,
                'number' => 3,
                'subject_teacher_id' => 11,
                'schedule_id' => 1,
                'classroom' => 11,
                'class_department_id' => 1,
                'day_number' => 4,
                'created_at' => '2018-10-18 22:39:15',
                'updated_at' => '2018-10-18 22:39:15',
            ),
            24 => 
            array (
                'id' => 42,
                'number' => 4,
                'subject_teacher_id' => 10,
                'schedule_id' => 1,
                'classroom' => 15,
                'class_department_id' => 1,
                'day_number' => 4,
                'created_at' => '2018-10-18 22:39:43',
                'updated_at' => '2018-10-18 22:39:43',
            ),
            25 => 
            array (
                'id' => 43,
                'number' => 1,
                'subject_teacher_id' => 13,
                'schedule_id' => 1,
                'classroom' => 200,
                'class_department_id' => 1,
                'day_number' => 5,
                'created_at' => '2018-10-18 22:39:54',
                'updated_at' => '2018-10-18 22:39:54',
            ),
            26 => 
            array (
                'id' => 44,
                'number' => 2,
                'subject_teacher_id' => 12,
                'schedule_id' => 1,
                'classroom' => 203,
                'class_department_id' => 1,
                'day_number' => 5,
                'created_at' => '2018-10-18 22:40:02',
                'updated_at' => '2018-10-18 22:40:02',
            ),
            27 => 
            array (
                'id' => 45,
                'number' => 3,
                'subject_teacher_id' => 8,
                'schedule_id' => 1,
                'classroom' => 111,
                'class_department_id' => 1,
                'day_number' => 5,
                'created_at' => '2018-10-18 22:40:09',
                'updated_at' => '2018-10-18 22:40:09',
            ),
            28 => 
            array (
                'id' => 46,
                'number' => 4,
                'subject_teacher_id' => 7,
                'schedule_id' => 1,
                'classroom' => 113,
                'class_department_id' => 1,
                'day_number' => 5,
                'created_at' => '2018-10-18 22:40:15',
                'updated_at' => '2018-10-18 22:40:15',
            ),
        ));
        
        
    }
}