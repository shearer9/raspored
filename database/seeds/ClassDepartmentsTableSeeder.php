<?php

use Illuminate\Database\Seeder;

class ClassDepartmentsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('class_departments')->delete();
        
        \DB::table('class_departments')->insert(array (
            0 => 
            array (
                'id' => 1,
                'department' => 'I.a',
                'created_at' => '2018-10-17 15:37:08',
                'updated_at' => '2018-10-17 15:37:08',
            ),
            1 => 
            array (
                'id' => 2,
                'department' => 'II.b',
                'created_at' => '2018-10-17 16:23:50',
                'updated_at' => '2018-10-17 16:23:50',
            ),
            2 => 
            array (
                'id' => 3,
                'department' => 'III.a',
                'created_at' => '2018-10-17 17:15:25',
                'updated_at' => '2018-10-17 17:15:25',
            ),
        ));
        
        
    }
}