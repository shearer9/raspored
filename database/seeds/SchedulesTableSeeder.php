<?php

use Illuminate\Database\Seeder;

class SchedulesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('schedules')->delete();
        
        \DB::table('schedules')->insert(array (
            0 => 
            array (
                'id' => 1,
                'date_from' => '2018-10-15',
                'date_to' => '2018-10-21',
                'shift' => 1,
                'created_at' => '2018-10-17 15:37:01',
                'updated_at' => '2018-10-17 15:37:01',
            ),
            1 => 
            array (
                'id' => 2,
                'date_from' => '2018-10-08',
                'date_to' => '2018-10-14',
                'shift' => 1,
                'created_at' => '2018-10-17 17:37:41',
                'updated_at' => '2018-10-17 17:37:41',
            ),
            2 => 
            array (
                'id' => 3,
                'date_from' => '2018-10-01',
                'date_to' => '2018-10-07',
                'shift' => 2,
                'created_at' => '2018-10-17 17:38:03',
                'updated_at' => '2018-10-17 17:38:03',
            ),
            3 => 
            array (
                'id' => 4,
                'date_from' => '2018-09-03',
                'date_to' => '2018-09-09',
                'shift' => 1,
                'created_at' => '2018-10-17 19:31:51',
                'updated_at' => '2018-10-17 19:31:51',
            ),
        ));
        
        
    }
}