<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Ivan',
                'address' => 'Mostar',
                'phone' => '232323',
                'email' => 'admin@admin.com',
                'password' => '$2y$10$vXuoai9KaPx501o34xBUgutAXuPNcD.aHY4qanf1zPnyb8VFo8CnS',
                'email_verified_at' => NULL,
                'admin' => 1,
                'remember_token' => 'FlVbatTvLJH1gY0AaWiGbVvrA1IeetEdkqtRp3V5iUcluU8hPj4aVPkn5EEz',
                'created_at' => '2018-10-08 12:36:23',
                'updated_at' => '2019-10-04 13:35:57',
            ),
            1 => 
            array (
                'id' => 3,
                'name' => 'Filip Filipović',
                'address' => 'Grude 123',
                'phone' => '232323',
                'email' => 'a@a.com',
                'password' => '$2y$10$CRP7F9PlrM5C1KUtQDQwBOg836FIRE0o29ZXMQQrb9Sb.ODnkpPfu',
                'email_verified_at' => NULL,
                'admin' => 0,
                'remember_token' => '0is5xFHMDlx3Hbz80rFIpVgEgTO2zcWoKIOBIGUAWduNuQOoD0YvAeuJf1Ud',
                'created_at' => '2018-10-16 23:40:43',
                'updated_at' => '2018-10-16 23:40:43',
            ),
            2 => 
            array (
                'id' => 4,
                'name' => 'Luka Lučić',
                'address' => 'Ante Starcevica',
                'phone' => '111222',
                'email' => 'a@aa.com',
                'password' => '$2y$10$QAnBpRaZV2KdFv89uadm0OOFiZ6ZTlkdj6Ixj1gZ3QtxuGvQpCvNK',
                'email_verified_at' => NULL,
                'admin' => 0,
                'remember_token' => NULL,
                'created_at' => '2018-10-16 23:45:09',
                'updated_at' => '2018-10-16 23:45:09',
            ),
            3 => 
            array (
                'id' => 5,
                'name' => 'Ivana Ivanović',
                'address' => 'Mostar 123',
                'phone' => '111222',
                'email' => 'b@b.com',
                'password' => '$2y$10$4rCpPuyTqlyFrsCNv/XgaOEgM9jes3E9hBU/5a28I8LCuQpR.W8Cm',
                'email_verified_at' => NULL,
                'admin' => 0,
                'remember_token' => 'YGEZVdhB7HxY4kOSsKQQWLNkkiOsW0Xnnv9t8oYmsSxvRpaDPyZYf6Tq3sRv',
                'created_at' => '2018-10-16 23:47:34',
                'updated_at' => '2018-10-16 23:47:34',
            ),
            4 => 
            array (
                'id' => 6,
                'name' => 'Jozo Jozić',
                'address' => 'Sarajevo 123',
                'phone' => '232323',
                'email' => 'd@d.com',
                'password' => '$2y$10$zmcfxSgONWVqr9IKttSxD.J.TA/aAMo8EtZz1rRpvuiWUo2EvDxt2',
                'email_verified_at' => NULL,
                'admin' => 0,
                'remember_token' => 'GXLprUZuFTcD0jFWAw6pgeP4IM975XqQQ4ziQtjdXfyRii2bHYSU9mEuoCAY',
                'created_at' => '2018-10-16 23:48:17',
                'updated_at' => '2018-10-16 23:48:17',
            ),
            5 => 
            array (
                'id' => 7,
                'name' => 'Marta Jozić',
                'address' => 'Mostar 123',
                'phone' => '222333',
                'email' => 'f@f.com',
                'password' => '$2y$10$ylknjoJxMMfHB6PDHXF33uKuYR/9sNVpDc4aDx9gUk5ZbNRvF6Vti',
                'email_verified_at' => NULL,
                'admin' => 0,
                'remember_token' => 'FbO4Wj9qUmp1q9CkiEJcU6sr57hCoD7Rxd44ceZ3C42aWSuGFvGaNrXifKx1',
                'created_at' => '2018-10-16 23:49:02',
                'updated_at' => '2018-10-16 23:49:02',
            ),
            6 => 
            array (
                'id' => 8,
                'name' => 'Ivano Ivanović',
                'address' => 'Sarajevo 123',
                'phone' => '232323',
                'email' => 'd@da.com',
                'password' => '$2y$10$x9i4RATrWEKazMygoywwf.3xh1cGoLJpal..NK5l94nQTlS7lGyj6',
                'email_verified_at' => NULL,
                'admin' => 0,
                'remember_token' => 'dTxmhU4bEsN3G0K7lLTvm4AtjAuP1KpN7DqjjMkaZEj8LYctZqwDkvuzBoOT',
                'created_at' => '2018-10-16 23:49:53',
                'updated_at' => '2018-10-16 23:49:53',
            ),
            7 => 
            array (
                'id' => 9,
                'name' => 'Miro Marjanović',
                'address' => 'Ante Starcevica',
                'phone' => '111111',
                'email' => 'g@g.com',
                'password' => '$2y$10$waNAxs3aow7cj3bHSes/bexqxXDJRBFA81BM3zSUQSqaVD.tWR91q',
                'email_verified_at' => NULL,
                'admin' => 0,
                'remember_token' => 'NRgg75AAkkdNYeCoqbjPlvZEimxIdKUTeoS7F2Sk9QPq2ypb761siWoxdT3P',
                'created_at' => '2018-10-16 23:50:54',
                'updated_at' => '2018-10-16 23:50:54',
            ),
            8 => 
            array (
                'id' => 10,
                'name' => 'Iva Jozić',
                'address' => 'Sarajevo 123',
                'phone' => '111222',
                'email' => 'g@gaa.com',
                'password' => '$2y$10$7mAiOb1K2.zIaiz5Ll7bP.vLHG34AGkWC6O7TJQe3PmEAEsqh7B3i',
                'email_verified_at' => NULL,
                'admin' => 0,
                'remember_token' => 'YLuCWXfAgWNzQTwqcpaID29HWgRjxc5KEXxIuBiI0dUjiHNhwEQksyJEC7hd',
                'created_at' => '2018-10-16 23:51:46',
                'updated_at' => '2018-10-16 23:51:46',
            ),
            9 => 
            array (
                'id' => 11,
                'name' => 'Nikola Krešić',
                'address' => 'Sarajevo 123',
                'phone' => '111222',
                'email' => 'dad@da.com',
                'password' => '$2y$10$8qCzMcG2VfdvDsslYWogf.18j/3vigqE3zqipOOgpk6GgdVYtG7e2',
                'email_verified_at' => NULL,
                'admin' => 0,
                'remember_token' => 'QuK5iYodwomqD5wvm3xnRHAklYj3EJdmHjkHRiEfwSCEyjFiEbCqbHHHUYID',
                'created_at' => '2018-10-16 23:52:59',
                'updated_at' => '2018-10-16 23:52:59',
            ),
            10 => 
            array (
                'id' => 12,
                'name' => 'Matej Melo',
                'address' => 'Mostar 123',
                'phone' => '111222',
                'email' => 'test12@gmail.com',
                'password' => '$2y$10$ltGhFhkga.yfVp7qsK.B2ehmQ8xYlZIhKMnmrj7ir78TEt0WRY31S',
                'email_verified_at' => NULL,
                'admin' => 0,
                'remember_token' => NULL,
                'created_at' => '2018-10-16 23:54:24',
                'updated_at' => '2018-10-16 23:54:24',
            ),
            11 => 
            array (
                'id' => 13,
                'name' => 'Anja Ivić',
                'address' => 'Sarajevo 123',
                'phone' => '111222',
                'email' => 'ko@ko.com',
                'password' => '$2y$10$RqgcVADQIL9fSvNCdE3s..kjsyLf8Kw45w/fTMS6Ek0Rbwg3Eop/i',
                'email_verified_at' => NULL,
                'admin' => 0,
                'remember_token' => 'wNg07hBVtnHa0TTYBjdjXe0JGU1U6juzrT3e7XqG2LbmhDM7d9CcwaWHwV3p',
                'created_at' => '2018-10-17 15:55:29',
                'updated_at' => '2018-10-17 15:55:29',
            ),
        ));
        
        
    }
}