<?php

use Illuminate\Database\Seeder;

class TeachersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('teachers')->delete();
        
        \DB::table('teachers')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => 3,
                'created_at' => '2018-10-16 23:43:26',
                'updated_at' => '2018-10-16 23:43:26',
            ),
            1 => 
            array (
                'id' => 2,
                'user_id' => 4,
                'created_at' => '2018-10-16 23:45:19',
                'updated_at' => '2018-10-16 23:45:19',
            ),
            2 => 
            array (
                'id' => 3,
                'user_id' => 5,
                'created_at' => '2018-10-16 23:47:43',
                'updated_at' => '2018-10-16 23:47:43',
            ),
            3 => 
            array (
                'id' => 4,
                'user_id' => 6,
                'created_at' => '2018-10-16 23:48:20',
                'updated_at' => '2018-10-16 23:48:20',
            ),
            4 => 
            array (
                'id' => 5,
                'user_id' => 7,
                'created_at' => '2018-10-16 23:49:05',
                'updated_at' => '2018-10-16 23:49:05',
            ),
            5 => 
            array (
                'id' => 6,
                'user_id' => 8,
                'created_at' => '2018-10-16 23:50:01',
                'updated_at' => '2018-10-16 23:50:01',
            ),
            6 => 
            array (
                'id' => 7,
                'user_id' => 9,
                'created_at' => '2018-10-16 23:51:06',
                'updated_at' => '2018-10-16 23:51:06',
            ),
            7 => 
            array (
                'id' => 8,
                'user_id' => 10,
                'created_at' => '2018-10-16 23:52:04',
                'updated_at' => '2018-10-16 23:52:04',
            ),
            8 => 
            array (
                'id' => 9,
                'user_id' => 11,
                'created_at' => '2018-10-16 23:53:36',
                'updated_at' => '2018-10-16 23:53:36',
            ),
            9 => 
            array (
                'id' => 10,
                'user_id' => 12,
                'created_at' => '2018-10-16 23:55:09',
                'updated_at' => '2018-10-16 23:55:09',
            ),
            10 => 
            array (
                'id' => 11,
                'user_id' => 13,
                'created_at' => '2018-10-17 16:01:14',
                'updated_at' => '2018-10-17 16:01:14',
            ),
        ));
        
        
    }
}