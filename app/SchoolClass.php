<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchoolClass extends Model
{
    protected $fillable = [
        'number', 'subject_teacher_id', 'schedule_id', 'classroom', 'class_id', 'day_number', 'class_department_id'
    ];

    public function subjectTeacher()
    {
        return $this->belongsTo(SubjectTeacher::class);
    }

    public function schedule()
    {
        return $this->belongsTo(Schedule::class);
    }

    public function classDepartment()
    {
        return $this->belongsTo(ClassDepartment::class);
    }

    public static function teachersClasses($teacher, $scheduleId)
    {
        $array = SubjectTeacher::where('teacher_id', '=', $teacher)->pluck('id')->toArray();
        return self::where('schedule_id', '=', $scheduleId)->whereIn('subject_teacher_id', $array)->get();
    }

    public static function studentsClasses($class, $scheduleId)
    {
        return self::where('schedule_id', '=', $scheduleId)->where('class_department_id', '=', $class)->get();
    }
}
