<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $fillable = [
        'user_id'
    ];

    public function subjects()
    {
        return $this->belongsToMany(Subject::class)->using(SubjectTeacher::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
