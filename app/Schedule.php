<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $fillable = [
        'date_from', 'date_to', 'shift', 'confirmed'
    ];

    public function schoolClasses()
    {
         return $this->hasMany(SchoolClass::class);
    }
}
