<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassDepartment extends Model
{
    protected $fillable = [
        'department'
    ];

    public function schoolClasses()
    {
        return $this->hasMany(SchoolClass::class);
    }
}
