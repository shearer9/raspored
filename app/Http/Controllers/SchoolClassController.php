<?php

namespace App\Http\Controllers;

use App\SchoolClass;
use Illuminate\Http\Request;
use App\SubjectTeacher;
use App\ClassDepartment;

class SchoolClassController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $class = ClassDepartment::find($request->class);
        $schedule = $request->schedule;
        $subjectTeachers = SubjectTeacher::all();
        return view('schoolClasses.create', compact('class' ,'schedule', 'subjectTeachers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
        'number' => 'required',
        'day_number' => 'required',
        'subject_teacher_id' => 'required',
        'schedule_id' => 'required',
        'classroom' => 'required',
        'class_department_id' => 'required'
            ]);

        if(SchoolClass::where([
            'schedule_id' => $request->schedule_id,
            'number' => $request->number,
            'day_number' => $request->day_number,
            'class_department_id' => $request->class_department_id
        ])->exists())
            {
                return redirect()->back()->withErrors('Taj školski sat ili profesor su već zauzeti');
            }

        elseif(SchoolClass::where([
            'schedule_id' => $request->schedule_id,
            'number' => $request->number,
            'day_number' => $request->day_number,
            'subject_teacher_id' => $request->subject_teacher_id
        ])->exists())
            {
                return redirect()->back()->withErrors('Taj školski sat ili profesor su već zauzeti');
            }


        SchoolClass::create(request([
            'number',
            'day_number',
            'subject_teacher_id',
            'schedule_id',
            'classroom',
            'class_department_id'
        ]));

        session()->flash(
            'message', 'Uspješno ste dodali novi školski sat'
        );

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SchoolClass  $schoolClass
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolClass $schoolClass)
    {
        return view('schoolClasses.show', compact('schoolClass'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SchoolClass  $schoolClass
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolClass $schoolClass)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SchoolClass  $schoolClass
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SchoolClass $schoolClass)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SchoolClass  $schoolClass
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolClass $schoolClass)
    {
        $schoolClass->delete();

        return redirect('/schedules');
    }
}
