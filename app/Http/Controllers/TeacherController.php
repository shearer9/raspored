<?php

namespace App\Http\Controllers;

use App\Teacher;
use App\Schedule;
use App\Day;
use Carbon\Carbon;
use App\SubjectTeacher;
use App\SchoolClass;
use Illuminate\Http\Request;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $teacherId = auth()->user()->teacher->id;
        $raspored = Schedule::latest('date_from')->first();
        $skolskiSati = SchoolClass::teachersClasses($teacherId, $raspored->id);
        return view('teachers.index', compact('skolskiSati', 'raspored'));
    }
}
