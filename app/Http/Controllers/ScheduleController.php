<?php

namespace App\Http\Controllers;

use App\SchoolClass;
use App\Schedule;
use App\ClassDepartment;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function classSchedule(Request $request)
    {
        $schedule = Schedule::find($request->scheduleId);
        $currentClass = ClassDepartment::find($request->class);
        $schoolClasses = $schedule->schoolClasses()->where('class_department_id', $currentClass->id)->get();

        return view('schedules.class', compact('schoolClasses', 'schedule', 'currentClass'));
    }

    public function index()
    {
        $schedules = Schedule::latest('date_from')->get();
        return view('schedules.index', compact('schedules'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('schedules.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
        'date_from' => 'required',
        'date_to' => 'required',
        'shift' => 'required'
            ]);

        Schedule::create(request([
            'date_from',
            'date_to',
            'shift'
        ]));

        return redirect('/schedules');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function show(Schedule $schedule)
    {
        $classes = ClassDepartment::all();
        return view('schedules.classesSchedules', compact('classes'));
        //         $raspored = Schedule::latest('date_from')->first();
        // $skolskiSati = SchoolClass::studentsClasses($class, $raspored->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function edit(Schedule $schedule)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Schedule $schedule)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function destroy(Schedule $schedule)
    {
        $schedule->schoolClasses()->delete();
        $schedule->delete();

        return redirect('/schedules');
    }
}
