<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Schedule;
use App\Day;
use Carbon\Carbon;
use App\Subject;
use App\SubjectTeacher;
use App\SchoolClass;
use App\Teacher;
use App\ClassDepartment;
use Auth;
use Unirest;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin')->only('professors');
        $this->middleware('auth')->only(['subjectChoice', 'attachSubject']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response = Unirest\Request::get("https://numbersapi.p.mashape.com/random/trivia?fragment=true&json=true&max=20&min=10",
        array(
            "X-Mashape-Key" => "x5GTZa8MwymshZpDpSRnrm8rLiM4p1jDm9YjsnfqMTyLZvfBBV",
            "Accept" => "text/plain"));

        $quote = $response->body;
        $classDepartments = ClassDepartment::all();
        $raspored = Schedule::latest('date_from')->first();
        return view('home', compact('quote', 'classDepartments', 'raspored'));
    }

    public function studentsSchedule(Request $request)
    {
        $raspored = Schedule::latest('date_from')->first();
        $skolskiSati = SchoolClass::studentsClasses($request->class_department_id, $raspored->id);
        return view('studentsSchedule', compact('raspored', 'skolskiSati'));
    }

    public function professors()
    {
        $professors = Teacher::all();
        return view('professors', compact('professors'));
    }

    public function subjectChoice()
    {
        if(auth()->user()->teacher === null){
            $subjects = Subject::all();
            return view('subjectChoice', compact('subjects'));
        }else
        return redirect('/');
    }

    public function attachSubject(Request $request)
    {
        $teacher = Teacher::create(['user_id' => auth()->id()]);
        $teacher->subjects()->attach($request->subjects);
            return redirect('/');
    }
}
