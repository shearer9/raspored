<?php

namespace App\Http\Controllers;

use App\Subject;
use Illuminate\Http\Request;

class SubjectController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
    }


    public function store(Request $request)
    {
        $this->validate(request(), [
        'name' => 'required|string'
            ]);

        Subject::create(request(['name']));

        return redirect('/');
    }

}
