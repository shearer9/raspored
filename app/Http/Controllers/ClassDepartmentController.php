<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ClassDepartment;

class ClassDepartmentController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
    }


    public function store(Request $request)
    {
        $this->validate(request(), [
        'department' => 'required|string'
        ]);

        ClassDepartment::create(request(['department']));

        return redirect('/schedules');
    }
}
