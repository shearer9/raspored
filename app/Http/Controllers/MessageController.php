<?php

namespace App\Http\Controllers;

use Auth;
use App\Message;
use App\SchoolClass;
use Illuminate\Http\Request;

class MessageController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        //
    }

    public function create(SchoolClass $schoolClass)
    {
        if($schoolClass->subjectTeacher->teacher->user->id == Auth::id())
        {
            return view ('messages.create', compact('schoolClass'));
        }
        return response(null, 404);
    }

    public function store(Request $request, SchoolClass $schoolClass)
    {
        $this->validate(request(), [
        'text' => 'required'
        ]);

        $data = [
        'school_class_id' => $schoolClass->id,
        'text' => $request->text
        ];

        Message::create($data);

        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function show(Message $message)
    {
        //
    }
}
